import Config
import os
import os.path

class IndividualResultsManager(object):

    def __init__(self):
        
        self.distances = ("CERCA", "MEDIO", "LEJOS")
            
        self.initialTransformation = {    
            'RotateX': 0,
            'RotateY': 0,
            'RotateZ': 0,
            'TranslateX': 0,
            'TranslateY': 0,
            'TranslateZ': 0
        }
        self.initialSAD = -1
        self.initialSSD = -1
        
        self.continuousSolution = {    
            'RotateX': 0,
            'RotateY': 0,
            'RotateZ': 0,
            'TranslateX': 0,
            'TranslateY': 0,
            'TranslateZ': 0
        }
        self.continuousSAD = -1
        self.continuousSSD = -1
        
        self.discreteSolution = {    
            'RotateX': 0,
            'RotateY': 0,
            'RotateZ': 0,
            'TranslateX': 0,
            'TranslateY': 0,
            'TranslateZ': 0
        }
        self.discreteSAD = -1
        self.discreteSSD = -1
        
        self.refinedSolution = {    
            'RotateX': 0,
            'RotateY': 0,
            'RotateZ': 0,
            'TranslateX': 0,
            'TranslateY': 0,
            'TranslateZ': 0
        }
        self.refinedSAD = -1
        self.refinedSSD = -1
        
    def setInitialTransformation(self, parameters):
        self.initialTransformation['RotateX'] = parameters[0] 
        self.initialTransformation['RotateY'] = parameters[1]
        self.initialTransformation['RotateZ'] = parameters[2]
        self.initialTransformation['TranslateX'] = parameters[3]
        self.initialTransformation['TranslateY'] = parameters[4]
        self.initialTransformation['TranslateZ'] = parameters[5]
        
    def getInitialTransformation(self):
        transformation = []        
        transformation.append(self.initialTransformation['RotateX'])
        transformation.append(self.initialTransformation['RotateY'])
        transformation.append(self.initialTransformation['RotateZ'])
        transformation.append(self.initialTransformation['TranslateX'])
        transformation.append(self.initialTransformation['TranslateY'])
        transformation.append(self.initialTransformation['TranslateZ'])
        return transformation
        
    def setContinuousSolution(self, parameters):
        self.continuousSolution['RotateX'] = parameters[0] 
        self.continuousSolution['RotateY'] = parameters[1]
        self.continuousSolution['RotateZ'] = parameters[2]
        self.continuousSolution['TranslateX'] = parameters[3]
        self.continuousSolution['TranslateY'] = parameters[4]
        self.continuousSolution['TranslateZ'] = parameters[5]
        
    def getContinuousSolution(self):
        transformation = []        
        transformation.append(self.continuousSolution['RotateX'])
        transformation.append(self.continuousSolution['RotateY'])
        transformation.append(self.continuousSolution['RotateZ'])
        transformation.append(self.continuousSolution['TranslateX'])
        transformation.append(self.continuousSolution['TranslateY'])
        transformation.append(self.continuousSolution['TranslateZ'])
        return transformation
        
    def setDiscreteSolution(self, parameters):
        self.discreteSolution['RotateX'] = parameters[0] 
        self.discreteSolution['RotateY'] = parameters[1]
        self.discreteSolution['RotateZ'] = parameters[2]
        self.discreteSolution['TranslateX'] = parameters[3]
        self.discreteSolution['TranslateY'] = parameters[4]
        self.discreteSolution['TranslateZ'] = parameters[5]
    
    def getDiscreteSolution(self):
        transformation = []        
        transformation.append(self.discreteSolution['RotateX'])
        transformation.append(self.discreteSolution['RotateY'])
        transformation.append(self.discreteSolution['RotateZ'])
        transformation.append(self.discreteSolution['TranslateX'])
        transformation.append(self.discreteSolution['TranslateY'])
        transformation.append(self.discreteSolution['TranslateZ'])
        return transformation
        
    def setRefinedSolution(self, parameters):
        self.refinedSolution['RotateX'] = parameters[0] 
        self.refinedSolution['RotateY'] = parameters[1]
        self.refinedSolution['RotateZ'] = parameters[2]
        self.refinedSolution['TranslateX'] = parameters[3]
        self.refinedSolution['TranslateY'] = parameters[4]
        self.refinedSolution['TranslateZ'] = parameters[5]
    
    def getRefinedSolution(self):
        transformation = []        
        transformation.append(self.refinedSolution['RotateX'])
        transformation.append(self.refinedSolution['RotateY'])
        transformation.append(self.refinedSolution['RotateZ'])
        transformation.append(self.refinedSolution['TranslateX'])
        transformation.append(self.refinedSolution['TranslateY'])
        transformation.append(self.refinedSolution['TranslateZ'])
        return transformation
      
    def parse(self,nserie,nslice,index):
        #TODO: mejorar este parse
        directory = Config.INDIVIDUAL_TEST_PATH + "/" + str(nserie)
        if not index >= 0 or not index < len(self.distances):
            return False
        path = directory + "/" + "image_" + str(nslice + 1) + "_" + self.distances[index]
        if not os.path.exists(directory) or not os.path.isfile(path):
            return False
        with open(path,'r') as file: 
            for line in file:
            
                if line.startswith("initial_RotateX"):
                    self.initialTransformation['RotateX'] = float(line.replace("initial_RotateX = ", ""))
                if line.startswith("initial_RotateY"):
                   self.initialTransformation['RotateY'] = float(line.replace("initial_RotateY = ", ""))
                if line.startswith("initial_RotateZ"):
                   self.initialTransformation['RotateZ'] = float(line.replace("initial_RotateZ = ", ""))
                if line.startswith("initial_TranslateX"):
                   self.initialTransformation['TranslateX'] = float(line.replace("initial_TranslateX = ", ""))
                if line.startswith("initial_TranslateY"):
                    self.initialTransformation['TranslateY'] = float(line.replace("initial_TranslateY = ", ""))
                if line.startswith("initial_TranslateZ"):
                    self.initialTransformation['TranslateZ']= float(line.replace("initial_TranslateZ = ", ""))
                if line.startswith("initial_SAD"):
                    self.initialSAD = float(line.replace("initial_SAD = ", ""))
                if line.startswith("initial_SSD"):
                    self.initialSSD = float(line.replace("initial_SSD = ", ""))
                    
                if line.startswith("continuous_RotateX"):
                    self.continuousSolution['RotateX'] = float(line.replace("continuous_RotateX = ", ""))
                if line.startswith("continuous_RotateY"):
                   self.continuousSolution['RotateY'] = float(line.replace("continuous_RotateY = ", ""))
                if line.startswith("continuous_RotateZ"):
                   self.continuousSolution['RotateZ'] = float(line.replace("continuous_RotateZ = ", ""))
                if line.startswith("continuous_TranslateX"):
                   self.continuousSolution['TranslateX'] = float(line.replace("continuous_TranslateX = ", ""))
                if line.startswith("continuous_TranslateY"):
                    self.continuousSolution['TranslateY'] = float(line.replace("continuous_TranslateY = ", ""))
                if line.startswith("continuous_TranslateZ"):
                    self.continuousSolution['TranslateZ']= float(line.replace("continuous_TranslateZ = ", ""))
                if line.startswith("continuous_SAD"):
                    self.continuousSAD = float(line.replace("continuous_SAD = ", ""))
                if line.startswith("continuous_SSD"):
                    self.continuousSSD = float(line.replace("continuous_SSD = ", ""))
                        
                if line.startswith("discrete_RotateX"):
                    self.discreteSolution['RotateX'] = float(line.replace("discrete_RotateX = ", ""))
                if line.startswith("discrete_RotateY"):
                   self.discreteSolution['RotateY'] = float(line.replace("discrete_RotateY = ", ""))
                if line.startswith("discrete_RotateZ"):
                   self.discreteSolution['RotateZ'] = float(line.replace("discrete_RotateZ = ", ""))
                if line.startswith("discrete_TranslateX"):
                   self.discreteSolution['TranslateX'] = float(line.replace("discrete_TranslateX = ", ""))
                if line.startswith("discrete_TranslateY"):
                    self.discreteSolution['TranslateY'] = float(line.replace("discrete_TranslateY = ", ""))
                if line.startswith("discrete_TranslateZ"):
                    self.discreteSolution['TranslateZ']= float(line.replace("discrete_TranslateZ = ", ""))
                if line.startswith("discrete_SAD"):
                    self.discreteSAD = float(line.replace("discrete_SAD = ", ""))
                if line.startswith("discrete_SSD"):
                    self.discreteSSD = float(line.replace("discrete_SSD = ", ""))
                            
                if line.startswith("refined_RotateX"):
                    self.refinedSolution['RotateX'] = float(line.replace("refined_RotateX = ", ""))
                if line.startswith("refined_RotateY"):
                   self.refinedSolution['RotateY'] = float(line.replace("refined_RotateY = ", ""))
                if line.startswith("refined_RotateZ"):
                   self.refinedSolution['RotateZ'] = float(line.replace("refined_RotateZ = ", ""))
                if line.startswith("refined_TranslateX"):
                   self.refinedSolution['TranslateX'] = float(line.replace("refined_TranslateX = ", ""))
                if line.startswith("refined_TranslateY"):
                    self.refinedSolution['TranslateY'] = float(line.replace("refined_TranslateY = ", ""))
                if line.startswith("refined_TranslateZ"):
                    self.refinedSolution['TranslateZ']= float(line.replace("refined_TranslateZ = ", ""))
                if line.startswith("refined_SAD"):
                    self.refinedSAD = float(line.replace("refined_SAD = ", ""))
                if line.startswith("refined_SSD"):
                    self.refinedSSD = float(line.replace("refined_SSD = ", ""))
        return True                    
                    
    def save(self,nserie,nslice,index):
        directory = Config.INDIVIDUAL_TEST_PATH + "/" + str(nserie)
        if not os.path.exists(directory):
            os.makedirs(directory)
        with open(directory + "/" + "image_" + str(nslice + 1) + "_" + self.distances[index],'w') as file: 
            file.write("TRANSFORMACION INICIAL:\n")
            for key,val in self.initialTransformation.iteritems():
                file.write("initial_"+key+" = "+str(val)+"\n")
            file.write("initial_SAD = "+str(self.initialSAD)+"\n")
            file.write("initial_SSD = "+str(self.initialSSD)+"\n")
            file.write("\n")
            file.write("SOLUCION CONTINUA:\n")
            for key,val in self.continuousSolution.iteritems():
                file.write("continuous_"+key+" = "+str(val)+"\n")
            file.write("continuous_SAD = "+str(self.continuousSAD)+"\n")
            file.write("continuous_SSD = "+str(self.continuousSSD)+"\n")
            file.write("\n")
            file.write("SOLUCION DISCRETA:\n")
            for key,val in self.discreteSolution.iteritems():
                file.write("discrete_"+key+" = "+str(val)+"\n")
            file.write("discrete_SAD = "+str(self.discreteSAD)+"\n")
            file.write("discrete_SSD = "+str(self.discreteSSD)+"\n")
            file.write("\n")
            file.write("SOLUCION REFINADA:\n")
            for key,val in self.refinedSolution.iteritems():
                file.write("refined_"+key+" = "+str(val)+"\n")
            file.write("refined_SAD = "+str(self.refinedSAD)+"\n")
            file.write("refined_SSD = "+str(self.discreteSSD)+"\n")
            file.write("\n")
            

        