# -*- coding: utf-8 -*-
import SimpleITK as sitk
import Config
from transformations import Rigid3D

class HeartDataSet:
    
    volume = sitk.ReadImage(Config.FILE_NAME3D)
    origin = (0,0,0)
    
    #transformaciones iniciales de cada serie
    initTransformations = (Rigid3D.Generate([0.15, 0.0, 0.15, 35, 25, 20]),
                           Rigid3D.Generate([0.15, 0.1, 0.15, 38, 25, 17]),
                           Rigid3D.Generate([0.1, 0.05, 0.05, 30, 20, 15]),
                           Rigid3D.Generate([0.15, 0.05, 0.05, 35, 25, 20]),
                           Rigid3D.Generate([-0.15, 0.05, -0.2, 39, 20, 55]),
                           Rigid3D.Generate([0.05, 0.1, -0.07, 25, 25, 35]),
                           Rigid3D.Generate([0.05, -0.1, 0.11, 35, 45, 45]),
                           Rigid3D.Generate([-0.02, -0.02, 0.05, 35, 45, 45]),
                           Rigid3D.Generate([-0.02, -0.02, -0.13, 35, 35, 45]),
                           Rigid3D.Generate([0.15, 0.0, 0.15, 35, 35, 45]))

    def getVolume(self):
        return self.volume
        
    def getVolumePath(self):
        return Config.FILE_NAME3D
    
    def getOrigin(self):
        return self.origin

    def getGoalSlice(self, nserie, nimage):
        input_path = Config.DATASET_PATH+"/"+str(nserie)+"/"+Config.SLICE_FILENAME+str(nimage+1)+".mhd"                       
        return sitk.ReadImage(input_path)
        
    def getGoalSlicePath(self, nserie, nimage):
        return Config.DATASET_PATH+"/"+str(nserie)+"/"+Config.SLICE_FILENAME+str(nimage+1)+".mhd"   

    def getGoalSliceGroundTruth(self, nserie, nimage):
        groundTruthPath = Config.DATASET_PATH+"/"+str(nserie)+"/"+Config.GROUND_TRUTH_FILENAME 
        with open(groundTruthPath) as file:
            lines = file.readlines()
            groundTruth = lines[nimage]
            groundTruth = groundTruth.split(",")
            for i in xrange(len(groundTruth)):
                groundTruth[i] = float(groundTruth[i].replace("'",""))
            return Rigid3D.Generate(groundTruth)    
        
    def getOutputPath(self, nserie, nimage, regName):
        return Config.OUTPUT_PATH+"/"+regName+"/"+str(nserie)+"/"+Config.SLICE_FILENAME+str(nimage+1)
        
    def getInitialTransformation(self, nserie):
        return self.initTransformations[nserie]
        
    def getNumberOfParameters(self):
        return len(self.initTransformations[0].GetParameters())
        
    def getNumberOfSeries(self):
        return 10
        
    def getNumberOfImagesXserie(self):
        return 20