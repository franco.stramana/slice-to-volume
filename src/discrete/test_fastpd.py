import numpy as np
from libfastpd import fastpd
import ipdb
import itertools

# This should be vectorized
def make_pairs_2d(nx, ny):
    pairs = []
    idx = np.arange(nx*ny).reshape(ny, nx)
    for u in xrange(nx):
        for v in xrange(ny):
            if u < nx-1:
                pairs.append([idx[v,u], idx[v, u+1]])

            if v < ny-1:
                pairs.append([idx[v,u], idx[v+1, u]])
    return np.array(pairs, dtype=np.int32)

def make_pairs(nvars):
    listvars = np.arange(nvars)
    pairs = list(itertools.combinations(listvars, 2))
    return np.array(pairs, dtype=np.int32)

#nx = 5
#ny = 4
nvars = 6
L = 5
N = nvars# nvars*(nvars-1)/2
nit = 100

# ATTENTION needs to be float32
u = np.ones((L, N), dtype=np.float32)
print "unarios: " + str(u)
b = 0.5*np.ones((L, L), dtype=np.float32)

for i in xrange(L):
    b[i, i] = 0

# for test
L0 = 4L
#u[L0, :] = 0
pairs = make_pairs(nvars)
print "pairs: " + str(pairs)
blist = [b]*pairs.shape[0]


#ipdb.set_trace()
labels = fastpd(u, blist, pairs, nit)
#assert np.all(np.equal(labels, L0))

# Now I change one binary table
#blist[10] = 1000*(1-b)

#labels = fastpd(u, blist, pairs, nit)
#assert not np.all(np.equal(labels, L0))

print "labels: " + str(labels)