The libfastpd wrapper for Python was written by Rafael Marini ( marinirfsilva@gmail.com ),
using Pybin11 and FastPD C++ code available at http://www.csd.uoc.gr/~komod/FastPD/

Copyright (C) Rafael Marini, 2016 

FastPD was written by Nikos Komodakis and can be downloaded for research purposes here:  http://www.csd.uoc.gr/~komod/FastPD/

Department of Computer Science,
University of Crete, Greece

Mathématiques Appliquées aux Systèmes (MAS),
Ecole Centrale de Paris, France

Copyright © 2009. All rights reserved.

THE WORK IS ONLY FOR RESEARCH AND NON-COMMERCIAL PURPOSES. THE OPTIMIZATION METHOD IS PROTECTED FROM SEVERAL INTERNATIONAL PENDING PATENT APPLICATIONS. IF YOU WOULD LIKE TO USE THIS SOFTWARE FOR COMMERCIAL PURPOSES OR LICENSING THE TECHNOLOGY, PLEASE CONTACT nikos.paragios@ecp.fr